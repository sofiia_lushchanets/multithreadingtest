//
//  SecondClass.m
//  MultithreadingTest
//
//  Created by Sofiia Luschanets' on 4/12/17.
//  Copyright © 2017 Sofiia Luschanets'. All rights reserved.
//

#import "SecondClass.h"
@interface SecondClass()
@end
@implementation SecondClass

- (void)createArray {
        self.arrayToEdit = [[NSMutableArray alloc] initWithCapacity:6];
        for (int i=0; i<6; i++) {
            [self.arrayToEdit addObject:[NSString stringWithFormat:@"object-%i", i]];
        }
}

- (void)replaceObjInArray1: (NSString*) stringToAdd {
    @synchronized (self.arrayToEdit) {
        for (int j = 0; j < self.arrayToEdit.count; j++) {
            NSString *currentObject = [self.arrayToEdit objectAtIndex:j];
            
            [self.arrayToEdit replaceObjectAtIndex:j withObject:[currentObject stringByAppendingFormat:@"-%@", stringToAdd]];
            
            NSLog(@"%@", [self.arrayToEdit objectAtIndex:j]);
        }
    }
}

- (void)replaceObjInArray2: (NSString*) stringToAdd {
    @synchronized (self.arrayToEdit) {
        for (int j = 0; j < self.arrayToEdit.count; j++) {
            NSString *currentObject = [self.arrayToEdit objectAtIndex:j];
            
            [self.arrayToEdit replaceObjectAtIndex:j withObject:[currentObject stringByAppendingFormat:@"+%@", stringToAdd]];
            
            NSLog(@"%@", [self.arrayToEdit objectAtIndex:j]);
        }

    }
}

@end
