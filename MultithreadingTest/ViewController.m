//
//  ViewController.m
//  MultithreadingTest
//
//  Created by Sofiia Luschanets' on 4/12/17.
//  Copyright © 2017 Sofiia Luschanets'. All rights reserved.
//

#import "ViewController.h"
#import "FirstClass.h"
#import "SecondClass.h"

@interface ViewController ()

@property (nonatomic, strong) NSString* string;
@property(nonatomic, strong)SecondClass* sc;

@end

@implementation ViewController

//---------------------------------------------------------------------TEST 1

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    NSThread* thread1 = [[NSThread alloc] initWithTarget:self selector:@selector(callString) object:nil];
//    thread1.name = @"thread 1";
//    
//    NSThread* thread2 = [[NSThread alloc] initWithTarget:self selector:@selector(callString) object:nil];
//    thread2.name = @"thread 2";
//    
//    [thread1 start];
//    [thread2 start];
//}

//- (void)callString{
//    NSLog(@"%@ is here", [[NSThread currentThread] name]);
//    @synchronized (self) {
//        NSLog(@"%@ started", [[NSThread currentThread] name]);
//        self.string = @"blablabla";
//        NSLog(@"%@ finished", [[NSThread currentThread] name]);
//    }
//}

//---------------------------------------------------------------------TEST 2

//- (void)viewDidLoad {
//    [super viewDidLoad];
//        NSThread* thread1 = [[NSThread alloc] initWithTarget:self selector:@selector(changeString:) object:@"1 string"];
//        thread1.name = @"thread 1";
//    
//        NSThread* thread2 = [[NSThread alloc] initWithTarget:self selector:@selector(changeString:) object:@"2 string"];
//        thread2.name = @"thread 2";
//    
//        [thread1 start];
//        [thread2 start];
//    
//}
//
//- (void)changeString:(NSString*)newString{
//    NSLog(@"%@ is here", [[NSThread currentThread] name]);
//    //@synchronized (self) {
//        NSLog(@"%@ started", [[NSThread currentThread] name]);
//        self.string = newString;
//        NSLog(@"%@", self.string);
//        NSLog(@"%@ finished", [[NSThread currentThread] name]);
//    //}
//    NSLog(@"%@", self.string);
//}

//---------------------------------------------------------------------TEST 3

//-(void)viewDidLoad{
//    [super viewDidLoad];
//
//    FirstClass* object = [[FirstClass alloc] init];
//    dispatch_queue_t queue = dispatch_queue_create(DISPATCH_QUEUE_SERIAL, 0); //if to use global - then order is unpredictable
//    dispatch_async(queue, ^{
//        NSLog(@"first started");
//        [object doSomething];
//        NSLog(@"first ended");
//
//    });
//    
//    dispatch_async(queue, ^{
//        NSLog(@"second started");
//        [object createSomething];
//        NSLog(@"second ended");
//    });
//    
//}

//---------------------------------------------------------------------TEST 4
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    FirstClass* object = [[FirstClass alloc] init];
//        NSThread* thread1 = [[NSThread alloc] initWithTarget:object selector:@selector(doSomething) object:nil];
//        thread1.name = @"thread 1";
//
//        NSThread* thread2 = [[NSThread alloc] initWithTarget:object selector:@selector(createSomething) object:nil];
//        thread2.name = @"thread 2";
//
//        [thread1 start];
//        [thread2 start];
//
//}

//---------------------------------------------------------------------TEST 5

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    FirstClass* object = [[FirstClass alloc] init];
//    FirstClass* object2 = [[FirstClass alloc] init];
//    NSObject* someObj = [[NSObject alloc]init];
//    NSThread* thread1 = [[NSThread alloc] initWithTarget:object selector:@selector(doSomethingWithObj:) object:someObj];
//    thread1.name = @"thread 1";
//    
//    NSThread* thread2 = [[NSThread alloc] initWithTarget:object2 selector:@selector(doSomethingWithObj:) object:someObj];
//    thread2.name = @"thread 2";
//    
//    [thread1 start];
//    [thread2 start];
//    
//}

//---------------------------------------------------------------------TEST 6
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    SecondClass* objectSC = [[SecondClass alloc] init];
//        NSThread* thread1 = [[NSThread alloc] initWithTarget:objectSC selector:@selector(doSomethingSC) object:nil];
//        thread1.name = @"thread 1";
//
//        NSThread* thread2 = [[NSThread alloc] initWithTarget:objectSC selector:@selector(createSomethingSC) object:nil];
//        thread2.name = @"thread 2";
//
//        [thread1 start];
//        [thread2 start];
//
//}

//---------------------------------------------------------------------TEST 4. The last task
- (void)viewDidLoad{
    [super viewDidLoad];
    SecondClass* obj = [[SecondClass alloc] init];
    [obj createArray];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            
            NSLog(@"Thread 1 started");

            [obj replaceObjInArray1:@"Thread 1"];
            
             NSLog(@"Thread 1 finished");
    
        });
    
        dispatch_async(queue, ^{
            NSLog(@"Thread 2 started");
            
            [obj replaceObjInArray1:@"Thread 2"];
            
            NSLog(@"Thread 2 finished");
        });

    
}

@end
