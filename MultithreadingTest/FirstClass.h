//
//  FirstClass.h
//  MultithreadingTest
//
//  Created by Sofiia Luschanets' on 4/12/17.
//  Copyright © 2017 Sofiia Luschanets'. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FirstClass : NSObject
- (void)doSomethingWithObj:(NSObject*)obj;
- (void)doSomething;
- (void)createSomething;
@end
