//
//  SecondClass.h
//  MultithreadingTest
//
//  Created by Sofiia Luschanets' on 4/12/17.
//  Copyright © 2017 Sofiia Luschanets'. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SecondClass : NSObject
@property(strong, nonatomic) NSMutableArray* arrayToEdit;

- (void)createArray;
- (void)replaceObjInArray1: (NSString*) stringToAdd;
- (void)replaceObjInArray2: (NSString*) stringToAdd;

@end
