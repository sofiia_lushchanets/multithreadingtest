//
//  FirstClass.m
//  MultithreadingTest
//
//  Created by Sofiia Luschanets' on 4/12/17.
//  Copyright © 2017 Sofiia Luschanets'. All rights reserved.
//

#import "FirstClass.h"
#import "ViewController.h"
@interface FirstClass()
@end

@implementation FirstClass

- (void)doSomethingWithObj:(NSObject*)obj{
    NSLog(@" %@ is here", [[NSThread currentThread] name]);
    @synchronized (obj) {
        NSLog(@" %@ started", [[NSThread currentThread] name]);
        NSLog(@"do something");
        NSLog(@" %@ ended", [[NSThread currentThread] name]);
    }
}

- (void)createSomething {
    @synchronized (self) {
        NSLog(@" %@ started", [[NSThread currentThread] name]);
        NSLog(@"create something");
        NSLog(@" %@ ended", [[NSThread currentThread] name]);
    
    }
}

- (void)doSomething {
    @synchronized (self) {
        NSLog(@" %@ started", [[NSThread currentThread] name]);
        NSLog(@"do something");
        NSLog(@" %@ ended", [[NSThread currentThread] name]);
    }
}

@end
